package by.vironit.training.danil.eshop.repository;

import by.vironit.training.danil.eshop.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
