package by.vironit.training.danil.eshop.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "addresses", schema = "eshop_project_schema")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "address_identity_generator")
    @SequenceGenerator(name = "address_identity_generator",
            sequenceName = "addresses_id_seq",
            schema = "eshop_project_schema",
            allocationSize = 1)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "address")
    private String address;

    @ManyToMany(mappedBy = "addresses")
    private List<Seller> sellers;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address1 = (Address) o;

        return address != null ? address.equals(address1.address) : address1.address == null;
    }

    @Override
    public int hashCode() {
        return address != null ? address.hashCode() : 0;
    }
}
